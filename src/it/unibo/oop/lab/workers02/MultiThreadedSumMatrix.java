package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that given a bidimensional matrix, returns the sum of all its elements.
 * */
public class MultiThreadedSumMatrix implements SumMatrix {

private final int nThreads;
private double totalSum;
	/**
	 * MultiThreadedSumMatrix constructor.
	 * @param n
	 * 		Defines the number of threads that should be used
	 * */
	public MultiThreadedSumMatrix(final int n) {
		nThreads = n;
	}

	private class Worker extends Thread {
		private final double[][] matrix;
		private final int startpos;
		private final int nelem;
		private double res;

		Worker(final double[][] m, final int s, final int n) {
			matrix = m;
			startpos = s;
			nelem = n;
		}

		@Override
		public void run() {
			int row = startpos / matrix[0].length;
			int col = startpos % matrix[1].length;
			for (int i = 0; i < nelem; i++) {
				res += matrix[row][col];
				col++;
				row++;
				if (row >= matrix[0].length) {
					row = 0;
				}
				if (col >= matrix[1].length) {
					col = 0;
				}
			}
		}

		public double getRes() {
			return res;
		}

	}

	/**
	 * @param matrix
	 * 			An arbitrary sized matrix
	 * 
	 * @return The sum of all its elements
	 * */
	@Override
	public double sum(final double[][] matrix) {
		final int matrixSize = matrix[0].length * matrix[1].length;
		final int size = matrixSize % nThreads + matrixSize / nThreads;
		final List<Worker> w = new ArrayList<>(nThreads);
		for (int i = 0; i < matrixSize; i += size) {
			w.add(new Worker(matrix, i, size));
		}

		for (final Worker wo : w) {
			wo.start();
		}

		for (final Worker wo : w) {
			try {
				wo.join();
				totalSum += wo.getRes();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return totalSum;
	}

}
