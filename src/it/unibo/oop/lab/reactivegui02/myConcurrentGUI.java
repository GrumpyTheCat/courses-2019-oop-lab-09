package it.unibo.oop.lab.reactivegui02;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class myConcurrentGUI extends JFrame {
	
	private static final long serialVersionUID = -1379063262476596152L;
	private JLabel displayCounter = new JLabel("0");
	private JButton BTNUp = new JButton("up");
	private JButton BTNDown = new JButton("down");
	private JButton BTNStop = new JButton("stop");
	protected boolean isAscending = true;
	private final double windowWidth = 0.2;
	private final double windowHeight = 0.2;
	
	public myConcurrentGUI() {
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int)(screenSize.getWidth() * windowWidth), (int)(screenSize.getHeight() * windowHeight));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel mainPanel = new JPanel();
		mainPanel.add(displayCounter);
		mainPanel.add(BTNUp);
		mainPanel.add(BTNDown);
		mainPanel.add(BTNStop);
		this.setContentPane(mainPanel);
		this.setVisible(true);
		
		Agent a = new Agent();
		new Thread(a).start();
		
		BTNUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isAscending = true;				
			}
			
		});
		
		BTNDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isAscending = false;
			}
			
		});
		
		BTNStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				a.setStop();
				BTNUp.setEnabled(false);
				BTNDown.setEnabled(false);
				BTNStop.setEnabled(false);
			}
			
		});
	}
	
	private class Agent implements Runnable{

		int counter = 0;
		boolean stop = false;
		
		@Override
		public void run() {
			
			while(!stop) {	
				try {	
						if(isAscending)
							counter++;
						else
							counter--;
					Thread.sleep(100);
				} 
					catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				displayCounter.setText(Integer.toString(counter));
			}			
		}
		
		private  void setStop() {
			this.stop = true;
		}
		
	}
	
}
