package it.unibo.oop.lab.reactivegui03;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * CEO
 */
public class AnotherConcurrentGUI extends JFrame {

	private JLabel displayCounter = new JLabel("0");
	private JButton BTNUp = new JButton("up");
	private JButton BTNDown = new JButton("down");
	private JButton BTNStop = new JButton("stop");
	private final double windowWidth = 0.2;
	private final double windowHeight = 0.2;
	private boolean isAscending = true;
	Agent a = new Agent();
	
	public AnotherConcurrentGUI() {
		JPanel mainPanel = new JPanel();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int)(screenSize.getWidth() * windowWidth), (int)(screenSize.getHeight() * windowHeight));
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		mainPanel.add(displayCounter);
		mainPanel.add(BTNUp);
		mainPanel.add(BTNDown);
		mainPanel.add(BTNStop);
		this.setContentPane(mainPanel);
		
		new Thread(a).start();
		
		Countdown c = new Countdown();
		new Thread(c).start();
		
		BTNUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isAscending = true;
			}
			
		});
		
		BTNDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				isAscending = false;
			}
			
		});
		
		BTNStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				a.setStop();
				BTNUp.setEnabled(false);
				BTNDown.setEnabled(false);
				BTNStop.setEnabled(false);
			}
			
		});
		this.setVisible(true);
		
	}
	
	private class Agent implements Runnable{
		
		int counter = 0;
		boolean stop = false;

		@Override
		public void run() {
			
			while(!stop) {
				try {
				if(isAscending)
					counter++;
				else
					counter--;

					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				displayCounter.setText(Integer.toString(counter));
				
			}
			
		}
		
		private void setStop() {
			stop = true;
		}
		
	}
	
	private class Countdown implements Runnable{

				
		@Override
		public void run() {
			try {
				Thread.sleep(10000);
				BTNStop.doClick();
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	
}
